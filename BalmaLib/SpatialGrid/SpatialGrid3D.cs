﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public interface ISpatialQuery3D
{
    Vector3 AABBFrom { get; set; }
    Vector3 AABBTo { get; set; }
    Func<Vector3, bool> Filter { get; set; }
}

public class SpatialGrid3D : MonoBehaviour
{

    public float x { get { return transform.position.x; } }
    public float y { get { return transform.position.y; } }
    public float z { get { return transform.position.z; } }

    public Vector3 cellDimensions;
    public Int3 dimensions;

    private Dictionary<GridEntity, Int3> lastPositions;
    private HashSet<GridEntity>[,,] buckets;

    readonly public Int3 Outside = new Int3(-1, -1, -1);
    readonly public GridEntity[] Empty = new GridEntity[0];

    [Range(0, 1)]
    public float alpha;

    void Awake()
    {

        lastPositions = new Dictionary<GridEntity, Int3>();
        buckets = new HashSet<GridEntity>[dimensions.x, dimensions.y, dimensions.z];

        for (int i = 0; i < dimensions.x; i++)
            for (int j = 0; j < dimensions.y; j++)
                for (int k = 0; k < dimensions.z; k++)
                    buckets[i, j, k] = new HashSet<GridEntity>();
        
        //Provisiorio?
        foreach (var ent in GameObject.FindObjectsOfType<GridEntity>())
            AddEntity(ent);

    }

    public void AddEntity(GridEntity entity)
    {
        entity.OnMove += UpdateEntity;
        UpdateEntity(entity);
    }

    bool InsideGrid(Int3 position)
    {
        return
            0 <= position.x && position.x < dimensions.x &&
            0 <= position.y && position.y < dimensions.y &&
            0 <= position.z && position.z < dimensions.z;
    }
    Int3 PositionInGrid(Vector3 position)
    {
        return new Int3(
            (position.x - x) / cellDimensions.x,
            (position.y - y) / cellDimensions.y,
            (position.z - z) / cellDimensions.z
        );
    }

    public void UpdateEntity(GridEntity entity)
    {
        var prevCell = lastPositions.ContainsKey(entity) ? lastPositions[entity] : Outside;
        var curCell = PositionInGrid(entity.transform.position);

        if (prevCell.Equals(curCell)) return;

        //Entity was previously inside the grid and it will move from there
        if (InsideGrid(prevCell))
        {
            buckets[prevCell.x, prevCell.y, prevCell.z].Remove(entity);
        }

        //Entity is now inside the grid, and just moved from prev cell, add it to the new cell
        if (InsideGrid(curCell))
        {
            buckets[curCell.x, curCell.y, curCell.z].Add(entity);
            lastPositions[entity] = curCell;
        }
        else
        {
            lastPositions.Remove(entity);
        }
    }

    public IEnumerable<GridEntity> Query(Vector3 aabbFrom, Vector3 aabbTo, Func<Vector3, bool> filter)
    {

        var from = new Vector3(Mathf.Min(aabbFrom.x, aabbTo.x), Mathf.Min(aabbFrom.y, aabbTo.y), Mathf.Min(aabbFrom.z, aabbTo.z));
        var to = new Vector3(Mathf.Max(aabbFrom.x, aabbTo.x), Mathf.Max(aabbFrom.y, aabbTo.y), Mathf.Max(aabbFrom.z, aabbTo.z));

        var fromCoord = PositionInGrid(from);
        var toCoord = PositionInGrid(to);

        //¡Ojo que clampea a 0,0 el Outside! TODO: Checkear cuando descartar el query si estan del mismo lado
        fromCoord = fromCoord.Clamp(Int3.Zero, dimensions);
        toCoord = toCoord.Clamp(Int3.Zero, dimensions);

        if (!InsideGrid(fromCoord) && !InsideGrid(toCoord))
            return Empty;

        var xrows = Generate(fromCoord.x, x => x + 1)
            .TakeWhile(x => x < dimensions.x && x <= toCoord.x);

        var yrows = Generate(fromCoord.y, y => y + 1)
            .TakeWhile(y => y < dimensions.y && y <= toCoord.y);

        var zrows = Generate(fromCoord.z, z => z + 1)
            .TakeWhile(z => z < dimensions.z && z <= toCoord.z);

        var cells = xrows.SelectMany(
            xs => yrows.SelectMany(
                ys => zrows.Select(
                    zs => new Int3(xs, ys, zs)
                )
            )
        );

        return cells
            .SelectMany(cell => buckets[cell.x, cell.y, cell.z])
            .Where(entity =>
                from.x <= entity.transform.position.x && entity.transform.position.x <= to.x &&
                from.y <= entity.transform.position.y && entity.transform.position.y <= to.y &&
                from.z <= entity.transform.position.z && entity.transform.position.z <= to.z
            ).Where(entity => filter(entity.transform.position));

    }

    public IEnumerable<GridEntity> Query(ISpatialQuery3D query)
    {
        return Query(query.AABBFrom, query.AABBTo, query.Filter);
    }

    void OnDrawGizmos()
    {
        //Horizontales
        for (int j = 0; j < dimensions.y + 1; j++)
        {
            var posy = y + j * cellDimensions.y;
            for (int k = 0; k < dimensions.z + 1; k++)
            {
                var posz = z + k * cellDimensions.z;
                Debug.DrawLine(new Vector3(x, posy, posz), new Vector3(x + dimensions.x * cellDimensions.x, posy, posz), new Color(1, 0, 0, alpha));
            }
        }
        //Verticales
        for (int i = 0; i < dimensions.x + 1; i++)
        {
            var posx = x + i * cellDimensions.x;
            for (int k = 0; k < dimensions.z + 1; k++)
            {
                var posz = z + k * cellDimensions.z;
                Debug.DrawLine(new Vector3(posx, y, posz), new Vector3(posx, y + cellDimensions.y * dimensions.y, posz), new Color(0, 1, 0, alpha));
            }
        }
        //Transversales
        for (int i = 0; i < dimensions.x + 1; i++)
        {
            var posx = x + i * cellDimensions.x;
            for (int j = 0; j < dimensions.y + 1; j++)
            {
                var posy = y + j * cellDimensions.y;
                Debug.DrawLine(new Vector3(posx, posy, z), new Vector3(posx, posy, z + cellDimensions.z * dimensions.z), new Color(0, 0, 1, alpha));
            }
        }
    }

    IEnumerable<T> Generate<T>(T seed, Func<T, T> mutate)
    {
        T accum = seed;
        while (true)
        {
            yield return accum;
            accum = mutate(accum);
        }
    }

    IEnumerable<int> CountForever()
    {
        return Generate(0, a => a + 1);
    }

    IEnumerable<int> MyRange(int start, int count)
    {
        for (int i = start; i < start + count; i++)
            yield return i;
    }


}