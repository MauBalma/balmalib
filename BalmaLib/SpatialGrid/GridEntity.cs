﻿using System;
using UnityEngine;

public class GridEntity : MonoBehaviour
{
    public SpatialGrid2D grid { get; protected set; }
    public event Action<GridEntity> OnMove = delegate { };

    public float radius;

    protected Vector3 _lastPosition;

    public bool draw = false;

    void Start()
    {
        grid = FindObjectOfType<SpatialGrid2D>();
        grid.AddEntity(this);

        _lastPosition = transform.position;
    }

    void Update()
    {
        if (transform.position != _lastPosition)
        {
            _lastPosition = transform.position;
            OnMove(this);
        }
    }
    
    void OnDrawGizmos()
    {
        if(!draw) return;
        DebugExtension.DrawCircle(transform.position, Vector3.forward, Color.cyan, radius);
    }

}