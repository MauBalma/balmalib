
using System;
using UnityEngine;

public class BaseQuery2D : ISpatialQuery2D
{
    public Vector2 AABBFrom { get; protected set; }
    public Vector2 AABBTo { get; protected set; }
    public Func<Vector2, float, bool> Filter { get; protected set; }

    public BaseQuery2D(Vector2 aabbFrom, Vector2 aabbTo, Func<Vector2, float, bool> filter)
    {
        AABBFrom = aabbFrom;
        AABBTo = aabbTo;
        Filter = filter;
    }

}

public class InsideGridQuery : BaseQuery2D
{
    public InsideGridQuery(SpatialGrid2D grid) : base(grid.origin, grid.end, (v,r) => true)
    {
    }
}

public abstract class TransformQuery : ISpatialQuery2D
{
    public abstract Vector2 AABBFrom { get; }
    public abstract Vector2 AABBTo { get; }

    public abstract Func<Vector2, float, bool> Filter { get; }

    protected Transform _center;

    public TransformQuery(Transform center)
    {
        _center = center;
    }

}

public class ReutilizableQuery : ReutilizableQuery
{
    protected Func<Vector2, Vector2> _from;
    protected Func<Vector2, Vector2> _to;

    protected Func<Vector2, float, bool> _filter;

    public ReutilizableQuery(Transform center, Func<Vector2, Vector2> from, Func<Vector2, Vector2> to, Func<Vector2, float, bool> filter) : base(center)
    {
        _from = from;
        _to = to;
        _filter = filter;
    }

    public override Vector2 AABBFrom { get { return _from(_center.position); } }

    public override Vector2 AABBTo { get { return _to(_center.position); } }

    public override Func<Vector2, float, bool> Filter { get { return _filter; } }
}
