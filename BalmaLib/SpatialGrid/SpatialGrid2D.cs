﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public interface ISpatialQuery2D
{
    Vector2 AABBFrom { get; }
    Vector2 AABBTo { get; }
    Func<Vector2, float, bool> Filter { get; }
}

public class SpatialGrid2D : MonoBehaviour
{

    public float x { get { return transform.position.x; } }
    public float y { get { return transform.position.y; } }

    public Vector2 origin { get { return transform.position; } }
    public Vector2 end { get { return origin + new Vector2(cellCant.x * cellDimensions.x, cellCant.y * cellDimensions.y); } }

    public Vector2 cellDimensions;
    public Int2 cellCant;

    private Dictionary<GridEntity, Int2> lastPositions;
    private HashSet<GridEntity>[,] buckets;

    static readonly public Int2 Outside = new Int2(-1, -1);
    readonly public GridEntity[] Empty = new GridEntity[0];

    void Awake()
    {
        lastPositions = new Dictionary<GridEntity, Int2>();
        buckets = new HashSet<GridEntity>[cellCant.x, cellCant.y];

        for (int i = 0; i < cellCant.x; i++)
            for (int j = 0; j < cellCant.y; j++)
                buckets[i, j] = new HashSet<GridEntity>();

    }

    public void AddEntity(GridEntity entity)
    {
        entity.OnMove += UpdateEntity;
        UpdateEntity(entity);
    }

    bool InsideGrid(Int2 position)
    {
        return
            0 <= position.x && position.x < cellCant.x &&
            0 <= position.y && position.y < cellCant.y;
    }
    Int2 PositionInGrid(Vector3 position)
    {
        return new Int2(
            (position.x - x) / cellDimensions.x,
            (position.y - y) / cellDimensions.y
        );
    }

    public void RemoveEntity(GridEntity entity)
    {
        var prevCell = lastPositions.ContainsKey(entity) ? lastPositions[entity] : Outside;
        if (InsideGrid(prevCell))
        {
            buckets[prevCell.x, prevCell.y].Remove(entity);
        }
        entity.OnMove -= UpdateEntity;
    }

    public void UpdateEntity(GridEntity entity)
    {
        var prevCell = lastPositions.ContainsKey(entity) ? lastPositions[entity] : Outside;
        var curCell = PositionInGrid(entity.transform.position);

        if (prevCell.Equals(curCell)) return;

        //Entity was previously inside the grid and it will move from there
        if (InsideGrid(prevCell))
        {
            buckets[prevCell.x, prevCell.y].Remove(entity);
        }

        //Entity is now inside the grid, and just moved from prev cell, add it to the new cell
        if (InsideGrid(curCell))
        {
            buckets[curCell.x, curCell.y].Add(entity);
            lastPositions[entity] = curCell;
        }
        else
        {
            lastPositions.Remove(entity);
        }
    }

    public IEnumerable<GridEntity> Query(Vector2 aabbFrom, Vector2 aabbTo, Func<Vector2, float, bool> filter)
    {
        var from = new Vector2(Mathf.Min(aabbFrom.x, aabbTo.x), Mathf.Min(aabbFrom.y, aabbTo.y));
        var to = new Vector2(Mathf.Max(aabbFrom.x, aabbTo.x), Mathf.Max(aabbFrom.y, aabbTo.y));

        var fromCoord = PositionInGrid(from);
        var toCoord = PositionInGrid(to);

        //¡Ojo que clampea a 0,0 el Outside! TODO: Checkear cuando descartar el query si estan del mismo lado
        fromCoord = fromCoord.Clamp(Int2.Zero, cellCant);
        toCoord = toCoord.Clamp(Int2.Zero, cellCant);

        if (!InsideGrid(fromCoord) && !InsideGrid(toCoord))
            return Empty;

        var xrows = EnumerableUtils.Generate(fromCoord.x, x => x + 1)
            .TakeWhile(x => x < cellCant.x && x <= toCoord.x);

        var yrows = EnumerableUtils.Generate(fromCoord.y, y => y + 1)
            .TakeWhile(y => y < cellCant.y && y <= toCoord.y);

        var cells = xrows.SelectMany(
            xs => yrows.Select(ys => new Int2(xs, ys))
        );

        return cells
            .SelectMany(cell => buckets[cell.x, cell.y])
            .Where(entity =>
                from.x <= entity.transform.position.x + entity.radius && entity.transform.position.x - entity.radius <= to.x &&
                from.y <= entity.transform.position.y + entity.radius && entity.transform.position.y - entity.radius <= to.y
            ).Where(entity => filter(entity.transform.position, entity.radius));

    }

    public IEnumerable<GridEntity> Query(ISpatialQuery2D query)
    {
        return Query(query.AABBFrom, query.AABBTo, query.Filter);
    }

    void OnDrawGizmos()
    {
        var cols = EnumerableUtils.Generate(x, curr => curr + cellDimensions.x)
            .Select(col => Tuple.Create(
                new Vector3(col, y, 0),
                new Vector3(col, y + cellDimensions.y * cellCant.y, 0)
                )
            );

        var rows = EnumerableUtils.Generate(y, curr => curr + cellDimensions.y)
            .Select(row => Tuple.Create(
                new Vector3(x, row, 0),
                new Vector3(x + cellDimensions.x * cellCant.x, row, 0)
                )
            );

        var allLines = cols
        .Take(cellCant.x + 1)
        .Concat(rows.Take(cellCant.y + 1));

        foreach (var line in allLines)
            Gizmos.DrawLine(line.Item1, line.Item2);
    }

}