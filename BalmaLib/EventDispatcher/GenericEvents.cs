using UnityEngine;

public interface GameEvent { }

// Permite pasarle a un objeto por editor que evento tiene que despachar, util con
// botones de UI.
public abstract class ScriptableEvent : ScriptableObject, GameEvent
{
    // pipi cucu
}

public abstract class ObjectEvent<T> : GameEvent
{
    public T obj { get; protected set; }
    public ObjectEvent(T obj)
    {
        this.obj = obj;
    }
}

public abstract class ScriptableObjectEvent<T> : ScriptableEvent
{
    public T obj { get; protected set; }
    public ScriptableObjectEvent(T obj)
    {
        this.obj = obj;
    }
}