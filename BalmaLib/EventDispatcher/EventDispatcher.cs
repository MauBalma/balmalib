﻿
using System.Collections.Generic;
using System;
using UnityEngine;

public class Events : Singleton<EventDispatcher>
{
    //Singleton magic	
}

public class EventDispatcher
{
    private Dictionary<Type, Delegate> delegates = new Dictionary<Type, Delegate>();

    public void Suscribe<T>(Action<T> callback) where T : GameEvent
    {
        var type = typeof(T);
        if (delegates.ContainsKey(type))
        {
            Delegate oldDelegate = delegates[type];

            delegates[type] = Delegate.Combine(oldDelegate, callback);
        }
        else
        {
            delegates[type] = callback;
        }
    }

    public void Unsuscribe<T>(Action<T> callback) where T : GameEvent
    {
        var type = typeof(T);
        if (delegates.ContainsKey(type))
        {
            var newDelegate = Delegate.Remove(delegates[type], callback);

            if (newDelegate == null)
            {
                delegates.Remove(type);
            }
            else
            {
                delegates[type] = newDelegate;
            }
        }
    }

    public void Dispatch(GameEvent e)
    {
        var type = e.GetType();
        if (delegates.ContainsKey(type))
        {
            delegates[type].DynamicInvoke(e);
        }       
        
        //Propagacion por la jerarquia.
        /*var baseType = type.BaseType;
        while (!baseType.IsAbstract)
        {
            if (delegates.ContainsKey(baseType)) delegates[baseType].DynamicInvoke(e);
            baseType = baseType.BaseType;
        }*/
        
    }

}
