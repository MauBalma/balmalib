﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MinHeap<TData, TPriotity> where TPriotity : IComparable
{
    private List<Tuple<TData, TPriotity>> _data;

    public bool IsEmpty { get { return _data.Count <= 0; } }

    public MinHeap()
    {
        _data = new List<Tuple<TData, TPriotity>>();
    }

    public void Insert(Tuple<TData, TPriotity> kvPair)
    {
        _data.Add(kvPair);

        var curIndex = _data.Count - 1;
        if (curIndex == 0) return;

        var parentIndex = (int)(curIndex - 1) / 2;

        while (_data[curIndex].Item2.CompareTo(_data[parentIndex].Item2) < 0)
        {
            Swap(curIndex, parentIndex);

            curIndex = parentIndex;
            parentIndex = (int)(curIndex - 1) / 2;
        }
    }

    public void Insert(TData data, TPriotity priority)
    {
        Insert(new Tuple<TData, TPriotity>(data, priority));
    }

    public Tuple<TData, TPriotity> ExtractPair()
    {
        var min = _data[0];

        _data[0] = _data[_data.Count - 1];
        _data.RemoveAt(_data.Count - 1);

        if (_data.Count == 0) return min;

        int curIndex = 0;
        int leftIndex = 1;
        int rightIndex = 2;
        int minorIndex;

        if (_data.Count > rightIndex)
            minorIndex = _data[leftIndex].Item2.CompareTo(_data[rightIndex].Item2) < 0 ? leftIndex : rightIndex;
        else if (_data.Count > leftIndex)
            minorIndex = leftIndex;
        else return min;

        while (_data[minorIndex].Item2.CompareTo(_data[curIndex].Item2) < 0)
        {
            Swap(minorIndex, curIndex);

            curIndex = minorIndex;
            leftIndex = curIndex * 2 + 1;
            rightIndex = curIndex * 2 + 2;

            if (_data.Count > rightIndex)
                minorIndex = _data[leftIndex].Item2.CompareTo(_data[rightIndex].Item2) < 0 ? leftIndex : rightIndex;
            else if (_data.Count > leftIndex)
                minorIndex = leftIndex;
            else return min;
        }

        return min;
    }

    public TData Extract()
    {
        return ExtractPair().Item1;
    }

    private void Swap(int i1, int i2)
    {
        var aux = _data[i1];
        _data[i1] = _data[i2];
        _data[i2] = aux;
    }

}
