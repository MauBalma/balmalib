﻿using System;

[Serializable]
public struct Int3
{
    public int x;
    public int y;
    public int z;

    public Int3(int x, int y, int z)
    {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public Int3(float x, float y, float z) : this((int)x, (int)y, (int)z)
    {
    }

    public override bool Equals(object obj)
    {
        if (obj == null || GetType() != obj.GetType())
        {
            return false;
        }
        var int3 = (Int3)obj;
        return this == int3;
    }

    public override int GetHashCode()
    {
        int hash = 17;
        hash = hash * 23 + x.GetHashCode();
        hash = hash * 23 + y.GetHashCode();
        hash = hash * 23 + z.GetHashCode();
        return hash;
    }

    public static bool operator ==(Int3 i1, Int3 i2)
    {
        return i1.x == i2.x && i1.y == i2.y && i1.z == i2.z;
    }

    public static bool operator !=(Int3 i1, Int3 i2)
    {
        return !(i1 == i2);
    }

    public static Int3 operator +(Int3 a, Int3 b)
    {
        return new Int3(a.x + b.x, a.y + b.y, a.z + b.z);
    }
    public static Int3 operator -(Int3 a)
    {
        return new Int3(-a.x, -a.y, -a.z);
    }
    public static Int3 operator -(Int3 a, Int3 b)
    {
        return -b + a;
        //return new Int3(a.x - b.x, a.y - b.y, a.z - b.z);
    }
    public static Int3 operator *(Int3 a, int d)
    {
        return new Int3(a.x * d, a.y * d, a.z * d);
    }
    public static Int3 operator *(int d, Int3 a)
    {
        return a * d;
    }
    public static Int3 operator /(Int3 a, float d)
    {
        return new Int3((int)(a.x / d), (int)(a.y / d), (int)(a.z / d));
    }

    public override String ToString()
    {
        return "(" + x + ";" + y + ";" + z + ")";
    }

    public Int3 Clamp(Int3 i1, Int3 i2)
    {
        return new Int3(
            x.Clamp(i1.x, i2.x),
            y.Clamp(i1.y, i2.y),
            z.Clamp(i1.z, i2.z));
    }

    public static Int3 Zero = new Int3(0,0,0);
}
