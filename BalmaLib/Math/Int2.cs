﻿using System;

[Serializable]
public struct Int2
{
    public int x;
    public int y;
    public Int2(int x, int y)
    {
        this.x = x;
        this.y = y;
    }

    public Int2(float x, float y) : this((int)x, (int)y)
    {
    }

    public override bool Equals(object obj)
    {
        if (obj == null || GetType() != obj.GetType())
        {
            return false;
        }
        var int2 = (Int2)obj;
        return this == int2;
    }

    public override int GetHashCode()
    {
        unchecked
        {
            int hash = 17;
            hash = hash * 23 + x.GetHashCode();
            hash = hash * 23 + y.GetHashCode();
            return hash;
        }

    }

    public static bool operator ==(Int2 i1, Int2 i2)
    {
        return i1.x == i2.x && i1.y == i2.y;
    }

    public static bool operator !=(Int2 i1, Int2 i2)
    {
        return !(i1 == i2);
    }

    public static Int2 operator +(Int2 a, Int2 b)
    {
        return new Int2(a.x + b.x, a.y + b.y);
    }
    public static Int2 operator -(Int2 a)
    {
        return new Int2(-a.x, -a.y);
    }
    public static Int2 operator -(Int2 a, Int2 b)
    {
        return -b + a;
        //return new Int2(a.x - b.x, a.y - b.y);
    }
    public static Int2 operator *(Int2 a, int d)
    {
        return new Int2(a.x * d, a.y * d);
    }
    public static Int2 operator *(int d, Int2 a)
    {
        return a * d;
    }
    public static Int2 operator /(Int2 a, float d)
    {
        return new Int2((int)(a.x / d), (int)(a.y / d));
    }

    public override String ToString()
    {
        return "(" + x + ";" + y +")";
    }

    public Int2 Clamp(Int2 i1, Int2 i2)
    {
        return new Int2(
            x.Clamp(i1.x, i2.x),
            y.Clamp(i1.y, i2.y));
    }

    public static Int2 Zero = new Int2(0, 0);

    public static Int2 Up = new Int2(0, 1);
    public static Int2 Right = new Int2(1, 0);
    public static Int2 Down = new Int2(0, -1);
    public static Int2 Left = new Int2(-1, 0);

    public static Int2 UpRight = new Int2(1, 1);
    public static Int2 RightDown = new Int2(1, -1);
    public static Int2 DownLeft = new Int2(-1, -1);
    public static Int2 LeftUp = new Int2(-1, 1);
}
