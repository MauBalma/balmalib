﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Task
{
    public enum STATE { PENDING, SUCCES, FAILURE }

	public STATE state = STATE.PENDING;
	
    public abstract STATE Update();

}
