using System;
using System.Collections.Generic;

public class Secuence : Task
{

    protected Queue<Task> pendent;
    protected Task current;

    public Secuence(Queue<Task> tasks)
    {
        pendent = tasks;
        current = tasks.Dequeue();
    }

    public override STATE Update()
    {
        if (state != STATE.PENDING) return state;

        var currentState = current.Update();

        //Si falla o esta pendiente 
        if (currentState != STATE.SUCCES) return currentState;

        // Si no quedan mas pendientes
        if (pendent.Count <= 0) return STATE.SUCCES;

        //La vida sigue
        current = pendent.Dequeue();
        return STATE.PENDING;
    }
}
