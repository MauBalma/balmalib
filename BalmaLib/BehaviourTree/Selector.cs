﻿using System;
using System.Collections.Generic;

public class Selector : Task
{

    protected Queue<Task> pendent;
    protected Task current;

    public Selector(Queue<Task> tasks)
    {
        pendent = tasks;
        current = tasks.Dequeue();
    }

    public override STATE Update()
    {
        if (state != STATE.PENDING) return state;

        var currentState = current.Update();

        //Si tiene exito o esta pendiente 
        if (currentState != STATE.FAILURE) return currentState;

        // Si no quedan mas pendientes
        if (pendent.Count <= 0) return STATE.FAILURE;

        //La vida sigue
        current = pendent.Dequeue();
        return STATE.PENDING;
    }
}
