﻿using System;
using System.Collections.Generic;

namespace FSM
{
    public class State
    {
        public string Name { get; protected set; }

        public event Action OnEnter = delegate { };
        public event Action OnUpdate = delegate { };
        public event Action OnExit = delegate { };

        protected List<Transition> _transitions;
        public IEnumerable<Transition> Transitions { get { return _transitions; } }

        public State(string name)
        {
            Name = name;
            _transitions = new List<Transition>();
        }
        
        public void AddTransition(Transition transition)
        {
            _transitions.Add(transition);
        }

        public void AddTransition(Func<bool> condition, State nextState)
        {
            _transitions.Add(new Transition(condition, nextState));
        }

        public void AddTransition(Func<bool> condition, Action OnTransition, State nextState)
        {
            var t = new Transition(condition, nextState);
            t.OnTransition += OnTransition;
            _transitions.Add(t);
        }

        public void Enter()
        {
            OnEnter();
        }
        public void Update()
        {
            OnUpdate();
        }
        public void Exit()
        {
            OnExit();
        }
    }
}