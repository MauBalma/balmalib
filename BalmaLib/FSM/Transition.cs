﻿using System;

namespace FSM
{
    public class Transition
    {
        public Func<bool> Condition { get; protected set; }
        public Action OnTransition = delegate { };
		public State NextState { get; protected set;}

		public Transition(Func<bool> condition, State nextState)
		{
			Condition = condition;
			NextState = nextState;
		}

		public void Execute()
		{
			OnTransition();
		}
    }
}