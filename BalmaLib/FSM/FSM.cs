﻿namespace FSM
{
    public class FiniteStateMachine
    {
        public State Current { get; protected set; }

        public FiniteStateMachine(State initialState)
        {
            Current = initialState;
            Current.Enter();
        }
        public void Update()
        {
            Current.Update();
        }

        public void CheckTransitions()
        {
            foreach (var transition in Current.Transitions)
            {
                if (transition.Condition())
                {
                    Current.Exit();
                    transition.Execute();
                    Current = transition.NextState;
                    Current.Enter();
                    return;
                }
            }
        }

        public void CheckAndUpdate()
        {
            CheckTransitions();
            Update();
        }
    }
}